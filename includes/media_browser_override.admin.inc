<?php
/**
 * @file
 * Contains the callback function that builds the admin form.
 */

/**
 * Page callback.
 */
function media_browser_override_admin_settings_form($form, &$form_state) {
  $form_state['data'] = array(
    'affected_file_types_property_prefix' => 'media_browser_dialog_options_affected_file_types_'
  );

  $form = array(
    'media_browser_dialog_options' => array(
      '#type' => 'fieldset',
      '#title' => t("Dialog Options"),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,

      'media_browser_override_default_wysiwyg_view_mode' => array(
        '#type' => 'checkbox',
        '#title' => t('Default to WYSIWYG Display Format?'),
        '#default_value' => variable_get('media_browser_override_default_wysiwyg_view_mode', 0),
        '#description' => t("Check this box if you want to default the inserted media view mode to the WYSIWYG view mode."),
      ),

      'media_browser_override_skip_view_mode_form' => array(
        '#type' => 'checkbox',
        '#title' => t('Skip View Mode Form?'),
        '#default_value' => variable_get('media_browser_override_skip_view_mode_form', 0),
        '#description' => t("Check this box if you want to skip the view mode form. Note: the default view mode will be used."),
      ),

      'media_browser_dialog_options_affected_file_types' => array(
        '#type' => 'fieldset',
        '#title' => t("Affected File Types"),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      ),
    ),

    'media_browser_override_alter_plugin_js' => array(
      '#type' => 'checkbox',
      '#title' => t('Alter Media Browser Plugin JS code?'),
      '#default_value' => variable_get('media_browser_override_alter_plugin_js', 0),
      '#description' => t("Check this box if you want to alter the Media Browser Plugin JS code. This will allow the the selected text in the WYSIWYG to populate the insterted media link text."),
    ),

    'media_browser_override_thumbnail_fix' => array(
      '#type' => 'checkbox',
      '#title' => t('Adjust media browser thumbnail?'),
      '#default_value' => variable_get('media_browser_override_thumbnail_fix', 0),
      '#description' => t("Check this box if you want to include css that alters the media browser thumbnails (makes them all the same height)."),
    ),

    'submit' => array( 
      '#type' => 'submit',
      '#value' => t('Save'),
    )
  );
  

  $file_types = file_type_load_all();
  $affected_file_types = _media_browser_override_get_affected_file_types();

  foreach ($file_types as $file_type => $file_type_object) :
    $default_value = _media_browser_override_check_file_type_affected($file_type);

    $form['media_browser_dialog_options']['media_browser_dialog_options_affected_file_types']["{$form_state['data']['affected_file_types_property_prefix']}{$file_type}"] = array(
      '#type' => 'checkbox',
      '#title' => t("Apply dialog options for {$file_type}?"),
      '#default_value' => $default_value,
    );
  endforeach;

  return $form;
}

/**
 * Callback function that saves form data
 */
function media_browser_override_admin_settings_form_submit($form, &$form_state) {
  variable_set('media_browser_override_default_wysiwyg_view_mode', $form_state['values']['media_browser_override_default_wysiwyg_view_mode']);
  variable_set('media_browser_override_skip_view_mode_form', $form_state['values']['media_browser_override_skip_view_mode_form']);
  variable_set('media_browser_override_alter_plugin_js', $form_state['values']['media_browser_override_alter_plugin_js']);
  variable_set('media_browser_override_thumbnail_fix', $form_state['values']['media_browser_override_thumbnail_fix']);

  $affected_file_types = array();

  foreach ($form_state['values'] as $key => $value) :
    if ( (substr($key, 0, strlen($form_state['data']['affected_file_types_property_prefix'])) === $form_state['data']['affected_file_types_property_prefix']) && ($value) ) :
      $affected_file_types[] = str_replace($form_state['data']['affected_file_types_property_prefix'], '', $key);
    endif;
  endforeach;

  _media_browser_override_set_affected_file_types($affected_file_types);
}
